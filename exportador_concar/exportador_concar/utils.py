from __future__ import unicode_literals
import frappe
from frappe.utils import cstr
import codecs
import os


def get_dates(year, periodo):
    if periodo == 'Enero':
        from_date = year + '-01-01'
        to_date = year + '-01-31'
    elif periodo == 'Febrero':
        from_date = year + '-02-01'
        to_date = year + '-02-29'
    elif periodo == 'Marzo':
        from_date = year + '-03-01'
        to_date = year + '-03-31'
    elif periodo == 'Abril':
        from_date = year + '-04-01'
        to_date = year + '-04-30'
    elif periodo == 'Mayo':
        from_date = year + '-05-01'
        to_date = year + '-05-29'
    elif periodo == 'Junio':
        from_date = year + '-06-01'
        to_date = year + '-06-30'
    elif periodo == 'Julio':
        from_date = year + '-07-01'
        to_date = year + '-07-31'
    elif periodo == 'Agosto':
        from_date = year + '-08-01'
        to_date = year + '-08-31'
    elif periodo == 'Setiembre':
        from_date = year + '-09-01'
        to_date = year + '-09-30'
    elif periodo == 'Octubre':
        from_date = year + '-10-10'
        to_date = year + '-10-31'
    elif periodo == 'Noviembre':
        from_date = year + '-11-01'
        to_date = year + '-11-30'
    elif periodo == 'Diciembre':
        from_date = year + '-12-01'
        to_date = year + '-12-31'
    return from_date, to_date

def file_name(year, periodo):
    if periodo == 'Enero':
        codigo_periodo = year + "01"
    elif periodo == 'Febrero':
        codigo_periodo = year + "02"
    elif periodo == 'Marzo':
        codigo_periodo = year + "03"
    elif periodo == 'Abril':
        codigo_periodo = year + "04"
    elif periodo == 'Mayo':
        codigo_periodo = year + "05"
    elif periodo == 'Junio':
        codigo_periodo = year + "06"
    elif periodo == 'Julio':
        codigo_periodo = year + "07"
    elif periodo == 'Agosto':
        codigo_periodo = year + "08"
    elif periodo == 'Setiembre':
        codigo_periodo = year + "09"
    elif periodo == 'Octubre':
        codigo_periodo = year + "610"
    elif periodo == 'Noviembre':
        codigo_periodo = year + "11"
    elif periodo == 'Diciembre':
        codigo_periodo = year + "12"
    return codigo_periodo


@frappe.whitelist()
def send_file_to_client(file, tipo, nombre):
    data = read_txt(file)
    frappe.response['result'] = cstr(data)
    frappe.response['type'] = tipo
    frappe.response['doctype'] = nombre


def read_txt(file):
    data = ""
    exported_file = codecs.open(file, 'r', encoding='utf-8')
    for line in exported_file:
        data = data + line
    return data


def to_file(data, nombre):
    my_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    my_path = os.path.join(my_path, "concar/")
    archivo = os.path.join(my_path, nombre)
    exported_file = codecs.open(archivo, "w", encoding='utf-8')
    nombre, ext = nombre.split(".")
    for row in data:
        exported_file.write(
            row['origen'] + "," +
            str(row['nro_voucher']) + "," +
            row['fecha'] + "," +
            row['moneda'] + "," +
            row['glosa_prin'].replace('\n', ' ') + "," +
            str(row['tipo_cambio']) + "," +
            row['tipo_conv'] + "," +
            row['flag_conv'] + "," +
            row['fecha_conv'] + "," +
            row['codigo_asiento'] + "," +
            row['codigo_anexo'] + "," +
            row['centro_costo'] + "," +
            str(row['debe_haber']) + "," +
            str(row['importe']) + "," +
            row['importe_dolar'] + "," +
            row['importe_sol'] + "," +
            row['codigo_comprobante'] + "," +
            row['numero_comprobante'] + "," +
            row['fecha_ven'] + "," +
            row['cod_area'] + "," +
            row['glosa_det'].replace('\n', ' ') + "," +
            row['codanexo'] + "," +
            row['tipo_pago'] + "," +
            row['tipo_doc_ref'] + "," +
            row['num_doc_ref'] + "," +
            row['fecha_doc_ref'] + "," +
            row['base_imp_ref'] + "," +
            row['igv_ref'] + "," +
            row['estado_mq'] + "," +
            row['num_serie_mq'] + "," +
            row['fecha_op'] + "," +
            row['tipo_tasa'] + "," +
            row['tasa_detper'] + "," +
            row['importe_base_detper_soles'] + "," +
            row['importe_base_detper_dolar'] + "\n")
    return {"archivo": archivo, "tipo": ext, "nombre": nombre}
