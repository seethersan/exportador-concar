# -*- coding: utf-8 -*-
# Copyright (c) 2018, OVENUBE and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import csv
from frappe.utils import cstr
import os
from frappe.model.document import Document

from exportador_concar.exportador_concar.utils import get_dates, file_name, to_file


class ExportarConcar(Document):
    pass


def get_account(year, periodo):
    account_list = []
    from_date, to_date = get_dates(year, periodo)

    account = frappe.db.sql("""select
                    IF(voucher_type = 'Purchase Invoice', '02', IF(voucher_type = 'Sales Invoice', '01', '03')) as origen,
                    (SELECT
                        COUNT(name)
                    FROM
                        `tabGL Entry` as gl_1
                    WHERE SUBSTRING(gl_1.name,3) <= SUBSTRING(gl.name,3)) nro_voucher,
                    DATE_FORMAT(gl.posting_date,'%d/%m/%Y') as fecha,
                    IF(gl.account_currency = 'SOL' OR 'PEN', 'S', 'D') as moneda,
                    gl.remarks as glosa_prin,
                    IFNULL(IF(voucher_type = 'Purchase Invoice',
                                            (select
                                                conversion_rate
                                            from
                                                `tabPurchase Invoice`where name=voucher_no),
                                            (select
                                                conversion_rate
                                            from
                                                `tabSales Invoice`
                                            where name=voucher_no)),'1.000') as tipo_cambio,
                    'F' as tipo_conv,
                    'S' as flag_conv,
                    IFNULL(IF(voucher_type = 'Purchase Invoice',
                                            (select
                                                DATE_FORMAT(due_date,'%d/%m/%Y')
                                            from
                                                `tabPurchase Invoice`where name=voucher_no),
                                            (select
                                                DATE_FORMAT(due_date,'%d/%m/%Y')
                                            from
                                                `tabSales Invoice`
                                            where name=voucher_no)),DATE_FORMAT(gl.posting_date,'%d/%m/%Y')) as fecha_conv,
                    SUBSTRING(gl.account,1,POSITION('-' in gl.account)-2) as codigo_asiento,
                    '0001' as codigo_anexo,
                    IFNULL(gl.cost_center,'') as centro_costo,
                    IF(gl.debit_in_account_currency = 0, 'H', 'D') as debe_haber,
                            IF(gl.debit_in_account_currency = 0, gl.credit_in_account_currency, gl.debit_in_account_currency) as importe,
                    '' as importe_dolar,
                    '' as importe_sol,
                    IF(voucher_type = 'Purchase Invoice',
                                            (select
                                                IF(LENGTH(codigo_comprobante) = 1, CONCAT('0', codigo_comprobante), codigo_comprobante)
                                            from
                                                `tabPurchase Invoice`where name=voucher_no),
                    IF(voucher_type = 'Sales Invoice',
                                            (select
                                                IF(LENGTH(codigo_comprobante) = 1, CONCAT('0', codigo_comprobante), codigo_comprobante)
                                            from
                                                `tabSales Invoice`
                                            where name=voucher_no),
                    IF(voucher_type = 'Delivery Note', (select
                                                IF(LENGTH(codigo_tipo_comprobante) = 1, CONCAT('0', codigo_tipo_comprobante), codigo_tipo_comprobante)
                                            from
                                                `tabDelivery Note`
                                            where name=voucher_no),
                    IF(voucher_type = 'Purchase Receipt', (select
                                                IF(LENGTH(codigo_tipo_comprobante) = 1, CONCAT('0', codigo_tipo_comprobante), codigo_tipo_comprobante)
                                            from
                                                `tabPurchase Receipt`
                                            where name=voucher_no),'')))) as codigo_comprobante,
                    CONCAT(IF(voucher_type = 'Purchase Invoice',IFNULL(
                                            (select
                                                bill_series
                                            from
                                                `tabPurchase Invoice`
                                            where name=voucher_no),''),
                                            SUBSTRING_INDEX(SUBSTRING_INDEX(voucher_no,'-',-2),'-',1)),'-',
                    IF(voucher_type = 'Purchase Invoice',
                                            (select
                                                bill_no
                                            from
                                                `tabPurchase Invoice`
                                            where name=voucher_no), SUBSTRING_INDEX(SUBSTRING_INDEX(voucher_no,'-',-2),'-',-1))) as numero_comprobante,
                    DATE_FORMAT(gl.posting_date,'%d/%m/%Y') as fecha_doc,
                    IFNULL(IF(voucher_type = 'Purchase Invoice',
                                            (select
                                                DATE_FORMAT(due_date,'%d/%m/%Y')
                                            from
                                                `tabPurchase Invoice`where name=voucher_no),
                                            (select
                                                DATE_FORMAT(due_date,'%d/%m/%Y')
                                            from
                                                `tabSales Invoice`
                                            where name=voucher_no)),DATE_FORMAT(gl.posting_date,'%d/%m/%Y')) as fecha_ven,
                    '' as cod_area,
                    gl.remarks as glosa_det,
                    '' as codanexo,
                            IF(voucher_type = 'Payment Entry',
                                (SELECT
                                    IF(mode_of_payment = "Cheque", '007',
                                        IF(mode_of_payment = "Efectivo", '008',
                                            IF(mode_of_payment = "Transferencia bancaria", '003',
                                                IF(mode_of_payment = "Giro Bancario", '002',''))))
                                FROM
                                    `tabPayment Entry` as pe
                                WHERE pe.name = gl.voucher_no), '') as tipo_pago,
                    '' as tipo_doc_ref,
                    '' as num_doc_ref,
                    '' as fecha_doc_ref,
                    '' as base_imp_ref,
                    '' as igv_ref,
                    '' as estado_mq,
                    '' as num_serie_mq,
                    DATE_FORMAT(gl.posting_date,'%d/%m/%Y') as fecha_op,
                    '' as tipo_tasa,
                    '' as tasa_detper,
                    '' as importe_base_detper_soles,
                    '' as importe_base_detper_dolar
                from
                    `tabGL Entry` gl
                where SUBSTRING(account,1,POSITION('-' in account)-1) > 100
                and posting_date > '""" + str(from_date) + """' 
                and posting_date < '""" + str(to_date) + """' 
                order by posting_date""", as_dict=True)

    for d in account:
        account_list.append({
            'origen': d.origen,
            'nro_voucher': d.nro_voucher,
            'fecha': d.fecha,
            'moneda': d.moneda,
            'glosa_prin': d.glosa_prin,
            'tipo_cambio': d.tipo_cambio,
            'tipo_conv': d.tipo_conv,
            'flag_conv': d.flag_conv,
            'fecha_conv': d.fecha_conv,
            'codigo_asiento': d.codigo_asiento,
            'codigo_anexo': d.codigo_anexo,
            'centro_costo': d.centro_costo,
            'debe_haber': d.debe_haber,
            'importe': d.importe,
            'importe_dolar': d.importe_dolar,
            'importe_sol': d.importe_sol,
            'codigo_comprobante': d.codigo_comprobante,
            'numero_comprobante': d.numero_comprobante,
            'fecha_ven': d.fecha_ven,
            'cod_area': d.cod_area,
            'glosa_det': d.glosa_det,
            'codanexo': d.codanexo,
            'tipo_pago': d.tipo_pago,
            'tipo_doc_ref': d.tipo_doc_ref,
            'num_doc_ref': d.num_doc_ref,
            'fecha_doc_ref': d.fecha_doc_ref,
            'base_imp_ref': d.base_imp_ref,
            'igv_ref': d.igv_ref,
            'estado_mq': d.estado_mq,
            'num_serie_mq': d.num_serie_mq,
            'fecha_op': d.fecha_op,
            'tipo_tasa': d.tipo_tasa,
            'tasa_detper': d.tasa_detper,
            'importe_base_detper_soles': d.importe_base_detper_soles,
            'importe_base_detper_dolar': d.importe_base_detper_dolar
        })
    return account_list


@frappe.whitelist()
def send_xls_to_client(periodo, year, ruc):
    nombre = "CONCAR" + ruc + file_name(year, periodo)
    data = get_account(year, periodo)

    csv_file = to_file(data, nombre + ".csv")
    filename = open(csv_file["archivo"])

    reader = csv.reader(filename)
    from frappe.utils.xlsxutils import make_xlsx
    xlsx_file = make_xlsx(reader, "Cuentas")

    filename.close()
    os.remove(csv_file["archivo"])

    frappe.response['filename'] = csv_file["nombre"] + '.xlsx'
    frappe.response['filecontent'] = xlsx_file.getvalue()
    frappe.response['type'] = 'binary'
