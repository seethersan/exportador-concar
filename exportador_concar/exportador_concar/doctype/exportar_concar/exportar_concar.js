// Copyright (c) 2018, OVENUBE and contributors
// For license information, please see license.txt

// Copyright (c) 2018, OVENUBE and contributors
// For license information, please see license.txt

frappe.provide("exportador_concar.exportar_concar");

frappe.ui.form.on('Exportar Concar', {
	refresh: function(frm) {

	}
});
frappe.ui.form.on('Exportar Concar', 'year', function(frm) {
	exportador_concar.exportar_concar.check_mandatory_to_set_button(frm);
	
});
frappe.ui.form.on('Exportar Concar', 'periodo', function(frm) {
	exportador_concar.exportar_concar.check_mandatory_to_set_button(frm);
	
});
frappe.ui.form.on('Exportar Concar', 'ruc', function(frm) {
	exportador_concar.exportar_concar.check_mandatory_to_set_button(frm);
	
});
frappe.ui.form.on('Exportar Concar', 'company', function(frm, cdt, cdn) {
	frappe.call({
		 	"method": "frappe.client.get",
            args: {
                doctype: "Company",
                name: frm.doc.company
            },
            callback: function (data) {
                if (data.message.company_name == null) {
                    
                }
                else{
                	frappe.model.set_value(frm.doctype, frm.docname, "ruc", data.message.tax_id);
                }
            }
        });
	exportador_concar.exportar_concar.check_mandatory_to_set_button(frm, cdt, cdn);
});
exportador_concar.exportar_concar.check_mandatory_to_set_button = function(frm) {
	if (frm.doc.periodo && frm.doc.ruc) {
		frm.fields_dict.get_data.$input.addClass("btn-primary");
	}
	else{
		frm.fields_dict.get_data.$input.removeClass("btn-primary");
	}
}
exportador_concar.exportar_concar.check_mandatory_to_fetch = function(doc) {
	$.each(["periodo"], function(i, field) {
		if(!doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
	});
	$.each(["company"], function(i, field) {
		if(!doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
	});
	$.each(["year"], function(i, field) {
		if(!doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
	});
}
frappe.ui.form.on("Exportar Concar", "get_data", function(frm) {
	exportador_concar.exportar_concar.check_mandatory_to_fetch(frm.doc);
	$(location).attr('href', "/api/method/exportador_concar.exportador_concar.doctype.exportar_concar.exportar_concar.send_xls_to_client?"+
				"periodo="+frm.doc.periodo+
				"&year="+frm.doc.year+
				"&ruc="+frm.doc.ruc);
});